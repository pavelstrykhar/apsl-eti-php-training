<?php

namespace Kaliop\Apsl\Eti\Validator;

abstract class AbstractValidator
{
    /**
     * @param $value
     * @return boolean|array true if OK, array with error description otherwise
     */
    abstract public function validate($value);
}
<?php

namespace Kaliop\Apsl\Eti\Form;

class SubmitField extends AbstractField
{
    /**
     * Renders field
     *
     * @param string $formName
     * @return string
     */
    public function render($formName = '')
    {
        return sprintf('<input type="submit" value="%s"/>', $this->value);
    }
}
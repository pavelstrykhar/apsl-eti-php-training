<?php

namespace Kaliop\Apsl\Eti\Form;


class SelectField extends AbstractField
{
    /**
     * Renders field
     *
     * @param string $formName
     * @return string
     */
    public function render($formName = '')
    {
        $str = sprintf('<label>%s</label>', $this->label);
        $str .= sprintf('<select name="%s">', $this->generateName($formName));

        foreach ($this->value as $key => $value) {
            $str .= sprintf('<option value="%s">%s</option>', $key, $value);
        }

        $str .= '</select>';

        return $str;
    }
}
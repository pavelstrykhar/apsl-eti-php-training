<?php

namespace Kaliop\Apsl\Eti\Controller;


use Kaliop\Apsl\Eti\DB\Manager;
use Kaliop\Apsl\Eti\Request\Request;
use Kaliop\Apsl\Eti\Response\Response;
use Kaliop\Apsl\Eti\Response\ViewResponse;
use Kaliop\Apsl\Eti\View\View;

abstract class Action
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Manager
     */
    protected $dbManager;

    /**
     * Action constructor
     */
    public function __construct()
    {
        $this->view = new View($this->getViewName());
    }

    /**
     * @return View
     */
    public function execute() {
        // TODO: do something for all actions before explicit action logic

        $response = $this->doExecute();

        // TODO: do something after ...

        // INFO: Jeżeli metoda doExecute zwróci Response to je zwracamy w innym przypadku potraktuj jako normalny widok
        return $response instanceof Response ? $response : new ViewResponse($this->view);
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getViewName()
    {
        if (preg_match('/^.*\\\\(.+?)Action$/', get_class($this), $matches)) {
            return strtolower($matches[1]);
        }

        throw new \Exception('Cannot resolve view name by class name');
    }

    /**
     * Action logic
     */
    protected abstract function doExecute();

    /**
     * @param Request $request
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * // INFO: Ustawienie menagera bazy danych dla akcji
     * @param Manager $dbManager
     */
    public function setDbManager(Manager $dbManager)
    {
        $this->dbManager = $dbManager;
    }
}

<?php

namespace Kaliop\Apsl\Eti\Util;


class Autoloader
{
    const DEFAULT_SUFFIX = '.php';

    /**
     * @var string
     */
    protected $libDir;

    /**
     * @var string
     */
    protected $suffix;

    /**
     * @param string $libDir
     * @param string $suffix
     */
    public function __construct($libDir = '', $suffix = self::DEFAULT_SUFFIX)
    {
        $this->libDir = $libDir;
        $this->suffix = $suffix;
    }

    /**
     * @param string $class
     */
    public function load($class) {
        $prefix = (!empty($this->libDir) ? $this->libDir . DIRECTORY_SEPARATOR : '');
        $fileName = $prefix . str_replace('\\', DIRECTORY_SEPARATOR, $class) . $this->suffix;

        if (file_exists($fileName)) {
            require $fileName;
        }
    }
}

<?php

namespace Kaliop\Apsl\Eti\DB;

interface MappingInterface
{
    /**
     * @return string
     */
    public function getIdColumnName();

    /**
     * @return string
     */
    public function getTableName();

    /**
     * [ db_col_name => property ]
     *
     * @return array
     */
    public function getFields();
}

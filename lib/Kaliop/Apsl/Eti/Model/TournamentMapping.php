<?php

namespace Kaliop\Apsl\Eti\Model;

use Kaliop\Apsl\Eti\DB\MappingInterface;

class TournamentMapping implements MappingInterface
{
    /**
     * @return string
     */
    public function getIdColumnName()
    {
        return 'id';
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'tournament';
    }

    /**
     * [ db_col_name => property ]
     *
     * @return array
     */
    public function getFields()
    {
        return array(
            'name' => 'name',
            'type' => 'type',
            'team_count' => 'teamCount'
        );
    }
}
